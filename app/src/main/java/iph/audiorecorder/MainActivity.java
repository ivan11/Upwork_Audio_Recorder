package iph.audiorecorder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.netcompss.loader.LoadJNI;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;
import net.rdrei.android.dirchooser.DirectoryChooserConfig;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import omrecorder.AudioChunk;
import omrecorder.AudioSource;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.Recorder;


public class MainActivity extends Activity {

    private static final int DEFAULT_FREQUENCY = 22050;
    private static final int DEFAULT_FORMAT = AudioFormat.ENCODING_PCM_16BIT;
    private static final String LOG_TAG = "AudioRecord";
    private static final int REQUEST_DIRECTORY = 1012;
    private String currentDirectory = null;

    public boolean needConvertToMp3;

    private Button recordButton = null;

    private TextView currentFolderPathView;

    private boolean isPlaying = false;
    private boolean isRecording = false;
    private ListView recordsView;
    private RadioGroup qualityView;

    private DateFormat RECORD_NAME_FORMAT = new SimpleDateFormat("hh_mm_ss dd_MM_yyyy");

    private List<Pair<String, Long>> records = new ArrayList<>();
    private Recorder recorder;
    private MediaPlayer player;
    private RadioGroup encodingView;
    private RadioGroup channelsView;


    public MainActivity() {

    }

    private void createDirectoriesIfNeeded() {

        currentDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();

        File folder = new File(currentDirectory, "AudioRecord");

        if (!folder.exists()) {
            folder.mkdir();
        }

        File audioFolder = new File(folder.getAbsolutePath(), "Audio");

        if (!audioFolder.exists()) {
            audioFolder.mkdir();
        }

        currentDirectory = audioFolder.getAbsolutePath();

    }

    private void showDirectoryPicker() {
        final Intent chooserIntent = new Intent(this, DirectoryChooserActivity.class);

        final DirectoryChooserConfig config = DirectoryChooserConfig.builder()
                .newDirectoryName("Audio")
                .allowReadOnlyDirectory(false)
                .allowNewDirectoryNameModification(true)
                .build();

        chooserIntent.putExtra(DirectoryChooserActivity.EXTRA_CONFIG, config);

        // REQUEST_DIRECTORY is a constant integer to identify the request, e.g. 0
        startActivityForResult(chooserIntent, REQUEST_DIRECTORY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_DIRECTORY) {
            if (resultCode == DirectoryChooserActivity.RESULT_CODE_DIR_SELECTED) {
                currentDirectory = data
                        .getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR);
                currentFolderPathView.setText(currentDirectory);
                setupRecorder();
                readAudioFilesFromDirectory();
            } else {
                // Nothing selected
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createDirectoriesIfNeeded();
        setContentView(R.layout.activity_main);
        initViews();
        setupRecorder();
        setUpListeners();

        readAudioFilesFromDirectory();
    }

    private void readAudioFilesFromDirectory() {
        new Thread() {
            @Override
            public void run() {
                File directory = new File(currentDirectory);
                records = new ArrayList<>();
                for (File file : directory.listFiles()) {
                    records.add(new Pair<String, Long>(file.getName(), file.length()));
                }
                showAudioFiles();
            }
        }.start();
    }

    private void showAudioFiles() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recordsView.setAdapter(new RecordsAdapter());
            }
        });
    }

    private class RecordsAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return records.size();
        }

        @Override
        public Object getItem(int position) {
            return records.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(MainActivity.this).inflate(R.layout.item_audio, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.recordName.setText(records.get(position).first);
            viewHolder.recordSize.setText(getStringSizeLengthFile(records.get(position).second));
            return convertView;
        }

        private class ViewHolder {
            private TextView recordName;
            private TextView recordSize;

            ViewHolder(View view) {
                recordName = (TextView) view.findViewById(R.id.tvRecordName);
                recordSize = (TextView) view.findViewById(R.id.tvRecordSize);
            }
        }

        public String getStringSizeLengthFile(long size) {

            DecimalFormat df = new DecimalFormat("0.00");

            float sizeKb = 1024.0f;
            float sizeMo = sizeKb * sizeKb;
            float sizeGo = sizeMo * sizeKb;
            float sizeTerra = sizeGo * sizeKb;


            if (size < sizeMo)
                return df.format(size / sizeKb) + " Kb";
            else if (size < sizeGo)
                return df.format(size / sizeMo) + " Mb";
            else if (size < sizeTerra)
                return df.format(size / sizeGo) + " Gb";

            return "";
        }

    }

    private void initViews() {
        recordButton = (Button) findViewById(R.id.record_button);
        currentFolderPathView = (TextView) findViewById(R.id.tvFolder);
        currentFolderPathView.setText(currentDirectory);
        recordsView = (ListView) findViewById(R.id.lvRecords);
        findViewById(R.id.btnChangeFolder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDirectoryPicker();
            }
        });
        recordsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (needConvertToMp3) {
                    needConvertToMp3 = false;
                    if (records.get(position).first.contains(".mp3")) {
                        Toast.makeText(MainActivity.this, "This file is already .mp3", Toast.LENGTH_SHORT).show();
                    } else {
                        convert(records.get(position).first);
                    }
                } else {
                    String recordToPlay = records.get(position).first;
                    stopPlaying();
                    isPlaying = !isPlaying;

                    recordButton.setEnabled(!isPlaying);

                    //startPlaying(recordToPlay);
                    playRecordingBinary(currentDirectory + "/" + recordToPlay);
                }

            }
        });

        findViewById(R.id.btnConvertToMp3).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Select record to convert", Toast.LENGTH_LONG).show();
                needConvertToMp3 = true;
            }
        });

        qualityView = (RadioGroup) findViewById(R.id.rgQuality);
        encodingView = (RadioGroup) findViewById(R.id.rgEncoding);
        channelsView = (RadioGroup) findViewById(R.id.rgChannels);
    }

    private void convert(String fileName) {
        fileName = currentDirectory + "/" + fileName;
        String fileOut = fileName.replace(".byte", ".mp3");

        ConvertBackground convertBackground = new ConvertBackground(
                this,
                new String[]{"-y", "-i", fileName,
                        "-ar", "22050", "-ac", "1", "-ab", "64k",
                        "-f", "mp3", fileOut},
                Environment.getExternalStorageDirectory().getPath()
                , fileName, fileOut);
        convertBackground.execute();
    }

    public class ConvertBackground extends AsyncTask<String, Integer, Integer> {

        private final String fileIn;
        private final String fileOut;
        ProgressDialog progressDialog;
        Context mContext;

        String[] command;
        String folder;

        public ConvertBackground(Context context, String[] command, String folder, String fileIn, String fileOut) {
            mContext = context;
            this.command = command;
            this.folder = folder;
            this.fileIn = fileIn;
            this.fileOut = fileOut;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Convert " + fileIn + " to mp3" +
                    "\n output file: " + fileOut);
            progressDialog.show();
        }

        protected Integer doInBackground(String... paths) {
            Log.i("convert", "convert doInBackground started...");

            LoadJNI vk = new LoadJNI();
            try {
                vk.run(command, folder, getApplicationContext());
            } catch (Throwable e) {
                Log.e("convert", "vk run exeption.", e);
            } finally {

            }

            Log.i("convert", "doInBackground finished");
            return Integer.valueOf(0);
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected void onCancelled() {
            Log.i("convert", "onCancelled");
            progressDialog.dismiss();
            super.onCancelled();
        }


        @Override
        protected void onPostExecute(Integer result) {
            Log.i("convert", "onPostExecute");
            progressDialog.dismiss();
            super.onPostExecute(result);

            Toast.makeText(MainActivity.this, "Finish", Toast.LENGTH_LONG).show();
            readAudioFilesFromDirectory();
        }

    }

    private void setUpListeners() {
        recordButton.setOnClickListener(recordClickListener);
    }


    private View.OnClickListener recordClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            isRecording = !isRecording;

            onRecord(isRecording);

            recordButton.setText(isRecording ? R.string.stop_recording : R.string.start_recording);
        }
    };

    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    private File getRecordFile() {
        String fileName = currentDirectory + "/" + RECORD_NAME_FORMAT.format(new Date()) + ".byte";
        return new File(fileName);
    }

    private void setupRecorder() {
        recorder = OmRecorder.pcm(
                new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                    @Override
                    public void onAudioChunkPulled(AudioChunk audioChunk) {
                        //  animateVoice((float) (audioChunk.maxAmplitude() / 200.0));
                    }
                }), getRecordFile());
    }

    private void startRecording() {
        recorder.startRecording();
    }

    private AudioSource mic() {
        return new AudioSource.Smart(MediaRecorder.AudioSource.MIC, getEncoding(),
                getChannels(), getSampleRate());
    }

    private int getChannels() {
        switch (channelsView.getCheckedRadioButtonId()) {
            case R.id.rbStereo:
                return AudioFormat.CHANNEL_IN_STEREO;
            case R.id.rbMono:
            default:
                return AudioFormat.CHANNEL_IN_MONO;
        }
    }

    private int getSampleRate() {
        switch (qualityView.getCheckedRadioButtonId()) {
            case R.id.rbHighQuality:
                return 32000;
            case R.id.rbMediumQuality:
                return 24000;
            case R.id.rbBasicQuality:
                return 16000;
            case R.id.rbSuperiorQuality:
            default:
                return 44100;
        }
    }

    private int getEncoding() {
        switch (encodingView.getCheckedRadioButtonId()) {
            case R.id.rbPcm8:
                return AudioFormat.ENCODING_PCM_8BIT;
            default:
            case R.id.rbPcm16:
                return AudioFormat.ENCODING_PCM_16BIT;
        }
    }

    void playRecord(final String fileName) {
        try {
            player = new MediaPlayer();
            player.reset();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);

            player.setDataSource(fileName);
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    player.start();
                }
            });
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    player.release();
                    stopPlaying();
                }
            });
            player.prepareAsync();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void playRecordingBinary(final String fileName) {
        new Thread() {
            @Override
            public void run() {
                byte[] audioData = null;

                try {
                    InputStream inputStream = new FileInputStream(fileName);

                    int minBufferSize = AudioTrack.getMinBufferSize(44100, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
                    audioData = new byte[minBufferSize];

                    AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, minBufferSize, AudioTrack.MODE_STREAM);
                    audioTrack.play();
                    int i = 0;

                    while ((i = inputStream.read(audioData)) != -1) {
                        audioTrack.write(audioData, 0, i);
                    }

                } catch (FileNotFoundException fe) {
                    Log.e(LOG_TAG, "File not found");
                } catch (IOException io) {
                    Log.e(LOG_TAG, "IO Exception");
                }
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stopPlaying();
                    }
                });
            }
        }.start();

    }


    private void stopRecording() {
        completionRecording();
        readAudioFilesFromDirectory();
    }

    private void stopPlaying() {
        completionPlaying();
    }

    private void completionRecording() {
        isRecording = false;
        recordButton.setText(R.string.start_recording);
        if (recorder != null) {
            try {
                recorder.stopRecording();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void completionPlaying() {
        isPlaying = false;
        recordButton.setEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();

        stopPlaying();

    }

    @Override
    public void onStop() {
        super.onStop();

        stopPlaying();

    }

}